# Pumplastique

Je suis parti d'un projet qui exécutait une web api et en front de l'angular 1.

Lien disponible ici : 

https://github.com/yizhaocs/springmvc4-angularjs-tomcat7-java6


Cela m'a permis de ne pas perdre de temps à mettre en place l'environnement.

Par la suite, j'ai procédé à une refonte et à une suppression de l'existant. Cela m'a permis de prendre propriété du projet.


Afin de gagner du temps sur l'implémentation, toutes les données sont stockées en mémoire. Mais il est possible de modifier facilement le projet pour ajouter une base de données.

# Pour tester

"npm run build" - dans le dossier "src/main/webapps/static" puis

"mvn clean install" - dans le dossier principal

Lancer le serveur toncat et ouvrer le navigateur http://localhost:8080/CarsAndGarages/



Dans le cas ou l'url changerais merci de faire le changement d'url pour l'application angular. Service : "src/main/webapps/static/app/service/webapi.service.ts".

Dans le dossier "src/main/webapps/static" lancer la commande "npm run build" et regénérer le projet java avec "mvn clean install".

Un nouveau fichier CarsAndGarages.war sera généré dans le target à deployer.


Une interface en ligne est disponible à l'adresse suivante : 

https://entretien-technique-pumplastique.prodgetonline.eu/

# Fichier war

Le fichier war est disponible dans la liste des téléchargements

# Explication

La vue a été réalisée avec angular4

La vue index recompilée a été rajoutée au serveur java afin de lancer le front au chargement de l'URL.

# Exécution

Pour exécuter le projet, il vous suffit de télécharger le war dans téléchargements, de le mettre dans le serveur tomcat et de lancer celui-ci.

Accéder à l'URL http://localhost:8080/CarsAndGarages/

# Réalisation
Ce projet a été réalisé pour l'entretien technique de l'entreprise Pumplastique.

Il a été fait en période d'examen. Seulement le nécessaire a été implémenté.

## Améliorations possibles
- Changer le chargement de toutes les voitures dans l'interface d'ajout de voiture dans un garage.
Actuellement, toutes les voitures sont chargées. Il faudrait rajouter des queryString sur la webapi pour les routes /garages et /cars (limite, valeur de recherche).
- Rajouter une connexion a une BD. Il suffirait juste de changer la classe Abstract JPA et les entités pour les mapper avec des annotations.
- rajouter sur l'interface utilisateur des messages d'indication.
Exemple: Si vous essayez de rajouter une voiture dans un garage alors que cette voiture est déjà dans un garage, la webapi retourne une erreur 409 conflits. Mais rien ne s'affiche pour l'utilisateur. (Pour un petit projet comme celui-ci la liste a été pensée pour être toujours à jour sauf dans le cas ou deux personnes viendraient à faire le même ajout de la même voiture dans un court laps de temps.)