import { PumplastiquePage } from './app.po';

describe('pumplastique App', () => {
  let page: PumplastiquePage;

  beforeEach(() => {
    page = new PumplastiquePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
