
import { Injectable } from '@angular/core';

import { WebApiService } from "app/service";
@Injectable()
export class CarService {
  url = "car";

  constructor(private webApiService: WebApiService) { }
  getAll() {
    return this.webApiService.request("get", this.url);
  }
  get(id) {
    return this.webApiService.request("get", this.url + "/" + id);
  }
  deleteGarage(car) {
    return this.webApiService.request("delete", "garage/" + car.garage + "/" + this.url, car);
  }
  delete(car) {
    return this.webApiService.request("delete", this.url + "/" + car.id);
  }
  update(car) {
    return this.webApiService.request("put", this.url + "/" + car.id, car);
  }
  create(car) {
    return this.webApiService.request("post", this.url, car);
  }
}