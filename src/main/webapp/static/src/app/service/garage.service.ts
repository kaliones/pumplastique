
import { Injectable } from '@angular/core';

import { WebApiService } from "app/service";
@Injectable()
export class GarageService {
  url = "garage";

  constructor(private webApiService: WebApiService) { }
  getAll() {
    return this.webApiService.request("get", this.url);
  }
  get(id)
  {
    return this.webApiService.request("get", this.url+"/"+id);
  }
  delete(garage)
  {    
    return this.webApiService.request("delete", this.url + "/" + garage.id);
  }
  update(garage) {
    return this.webApiService.request("put", this.url + "/" + garage.id, garage);
  }
  create(garage) {
    return this.webApiService.request("post", this.url, garage);
  }
  addCar(garage,car) {
    return this.webApiService.request("post", this.url + "/"+ garage.id+ "/car" , car);
  }
  removeCar(garage,car) {
    return this.webApiService.request("delete", this.url + "/"+ garage.id + "/car", car);
  }
}