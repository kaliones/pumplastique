
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/Rx";

@Injectable()
export class WebApiService {
  private static domaine = "http://localhost:8080/CarsAndGarages/";

  constructor(private http: Http) { }

  request(type, url,  body?) {
    if (type === 'get') {
      return (this.http.get(WebApiService.domaine + url, WebApiService.header()));//.map((response: Response) => callback(response));
    }
    else if (type === 'post') {
      return this.http.post(WebApiService.domaine + url, body, WebApiService.header());//.map((response: Response) => callback(response));
    }
    else if (type === 'delete') {
      //for delete, body is not an argument
      var option = WebApiService.header();
      option.body = body;
      return this.http.delete(WebApiService.domaine + url, option );//.map((response: Response) => callback(response));
    }
    else if (type === 'put') {
      return this.http.put(WebApiService.domaine + url, body, WebApiService.header());//.map((response: Response) => callback(response));
    }
    else if (type === 'patch') {
      return this.http.patch(WebApiService.domaine + url, body, WebApiService.header());//.map((response: Response) => callback(response));
    }
  }
  private static header() {
    let headers;
    headers = new Headers({ 'Content-Type': 'application/json' });

    return new RequestOptions({ headers: headers });
  }
}