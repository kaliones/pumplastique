import { Component, OnInit, Injectable } from '@angular/core';
import { GarageService } from "app/service";
@Component({
    moduleId: module.id,
    selector: 'garages',
    templateUrl: './garages.component.html',
    styleUrls: ['./garages.component.css']
})
@Injectable()
export class GaragesComponent implements OnInit {
    title = "PumCars - Garages List";
    garages: Array<Object> = [];
    hiddenForm = true;
    garage = {};
    min = 0;
    constructor(private garageService: GarageService) {

    }
    ngOnInit(): void {
        this.garageService.getAll().subscribe(
            data => {
                this.garages = data.json();
                this.sortGarage();
                console.log(this.garages);
            },
            error => {
            });
    }
    delete(garage) {
        this.garageService.delete(garage).subscribe(
            data => {
                this.garages.splice(this.garages.findIndex(garageSearch => garageSearch["id"] == garage.id), 1);
                this.sortGarage();
            },
            error => {
                //  console.log(error); 
            });

    }
    edit(garage) {
        this.hiddenForm = false;
        //for clone garage
        this.garage = JSON.parse(JSON.stringify(garage));
        this.min = this.garage["cars"].length;
    }
    new() {
        this.hiddenForm = false;
        this.garage = {};
        this.min = 0;
    }
    cancel() {
        this.hiddenForm = true;
        this.garage = {};
    }
    submit() {
        console.log("submit");
        if (this.garage["id"] >= 0) {
            this.garageService.update(this.garage).subscribe(
                data => {
                    var index = this.garages.findIndex(obj => obj["id"] == this.garage["id"]);
                    this.garages[index] = data.json();
                    this.sortGarage();
                },
                error => {
                    //  console.log(error);
                });
            this.hiddenForm = true;
        }
        else {
            this.garageService.create(this.garage).subscribe(
                data => {
                    console.log(data.json());
                    this.garages.push(data.json());
                    this.sortGarage();
                },
                error => {
                    //  console.log(error);
                });
            this.hiddenForm = true;
            this.garage = {};

        }
    }
    sortGarage() {
        this.garages.sort(
            (a, b) => {
                return a["id"] - b["id"];
            }
        );
    }

}