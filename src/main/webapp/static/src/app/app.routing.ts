import { Routes, RouterModule } from '@angular/router';

import { GaragesComponent } from './garages/index';
import { GarageComponent } from './garage/index';
import { CarComponent } from './car/index';

const appRoutes: Routes = [
  { path: 'garages', component: GaragesComponent },
  { path: 'cars', component: CarComponent },
  { path: 'garage/:id', component: GarageComponent },

  { path: '**', redirectTo: 'GarageComponent' }
];

export const routing = RouterModule.forRoot(appRoutes);