import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, Injectable } from '@angular/core';
import { GarageService, CarService } from "app/service";
@Component({
    moduleId: module.id,
    selector: 'garage',
    templateUrl: './garage.component.html',
    styleUrls: ['./garage.component.css']
})
@Injectable()
export class GarageComponent implements OnInit {
    title = "PumCars - Garages Detail";
    garage: Object = {};
    error = "Loading";
    cars: Array<Object> = [];
    allCars: Array<Object> = [];
    selectedCarAdd: Object = {};
    selectedCarDelete: Object = {};
    hiddenForm = true;
    addCarForm = false;
    constructor(private activatedRoute: ActivatedRoute, private garageService: GarageService, private carService: CarService) {

    }
    ngOnInit(): void {
        var id = -1;
        this.activatedRoute.params.subscribe((params: Params) => {
            id = params['id'];
        });
        this.garageService.get(id).subscribe(
            data => {
                this.garage = data.json();
                this.error = "";
                this.sortGarageCar();
            },
            error => {
                this.error = "Not Found";
            });
        this.carService.getAll().subscribe(
            data => {
                this.allCars = data.json();
                this.updateCar();
            },
            error => {
                this.error = "Error start page";
            });

    }
    deleteGarage(car) {
        this.carService.deleteGarage(car).subscribe(
            data => {
                this.allCars.find(obj => obj["id"] == car.id)["garage"] = -1;
                this.garage["cars"].splice(this.garage["cars"].findIndex(obj => obj["id"] == car.id), 1);
                this.sortGarageCar();
                this.updateCar();
                this.sortGarageCar();
                this.updateCar();
            },
            error => {
            });
    }
    addCar() {
        this.hiddenForm = false;
        this.addCarForm = true;
        this.cars = this.allCars.filter(car1 => car1["garage"] == -1 && !this.garage["cars"].some(cars2 => car1["id"] === cars2["id"]));

    }
    removeCar() {
        this.hiddenForm = false;
        this.addCarForm = false;
    }
    cancel() {
        this.hiddenForm = true;
    }
    submitAddCar(id) {
        console.log(id);
        var index = this.allCars.findIndex(obj => obj["id"] == id);
        this.garageService.addCar(this.garage, this.allCars[index]).subscribe(
            data => {
                this.garage = data.json();
                this.sortGarageCar();
                this.updateCar();
            },
            error => {
                //  console.log(error);
            });
        this.hiddenForm = true;
    }
    submitRemoveCar(id) {
        var index = this.garage["cars"].findIndex(obj => obj["id"] == id);
        this.garageService.removeCar(this.garage, this.garage["cars"][index]).subscribe(
            data => {
                this.allCars.find(obj => obj["id"] == id)["garage"] = -1;
                this.garage["cars"].splice(index, 1);
                this.sortGarageCar();
                this.updateCar();
            },
            error => {
                //  console.log(error);
            });
        this.hiddenForm = true;
    }
    sortGarageCar() {
        this.garage["cars"].sort(
            (a, b) => {
                return a["id"] - b["id"];
            }
        );
    }
    updateCar() {
        this.cars = this.allCars.filter(car1 => car1["garage"] == -1 && !this.garage["cars"].some(cars2 => car1["id"] === cars2["id"]));
        this.cars.sort(
            (a, b) => {
                return a["id"] - b["id"];
            }
        );
    }

}