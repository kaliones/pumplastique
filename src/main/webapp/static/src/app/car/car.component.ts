import { Component, OnInit, Injectable } from '@angular/core';
import { CarService } from "app/service";
@Component({
    moduleId: module.id,
    selector: 'car',
    templateUrl: './car.component.html',
    styleUrls: ['./car.component.css']
})
@Injectable()
export class CarComponent implements OnInit {
    title = "PumCars - Cars List";
    cars: Array<Object> = [];
    hiddenForm = true;
    car = {};
    constructor(private carService: CarService) {

    }
    ngOnInit(): void {
        this.carService.getAll().subscribe(
            data => {
                this.cars = data.json();
                this.sortCar();
            },
            error => {
                console.log(error);
                //this.alertService.error(error);
            });
    }
    delete(car) {
        this.carService.delete(car).subscribe(
            data => {
                data = data.json();
                this.cars.splice(this.cars.findIndex(carSearch => carSearch["id"] == car.id), 1);
                this.sortCar();
            },
            error => {
                console.log(error);
            });

    }
    deleteGarage(car) {
        this.carService.deleteGarage(car).subscribe(
            data => {
                car.garage = -1;
                this.sortCar();
            },
            error => {
                console.log(error);
            });

    }
    edit(car) {
        this.hiddenForm = false;
        //for clone garage
        this.car = JSON.parse(JSON.stringify(car));
    }
    new() {
        this.hiddenForm = false;
        this.car = {};
    }
    cancel() {
        this.hiddenForm = true;
        this.car = {};
    }
    submit() {
        console.log("submit");
        if (this.car["id"] >= 0) {
            this.carService.update(this.car).subscribe(
                data => {
                    var index = this.cars.findIndex(obj => obj["id"] == this.car["id"]);
                    this.cars[index] = data.json();
                    this.sortCar();
                },
                error => {
                    //  console.log(error);
                });
            this.hiddenForm = true;
        }
        else {
            this.carService.create(this.car).subscribe(
                data => {
                    this.cars.push(data.json());
                    this.sortCar();
                },
                error => {
                    //  console.log(error);
                });
            this.hiddenForm = true;
            this.car = {};

        }
    }
    sortCar() {
        this.cars.sort(
            (a, b) => {
                return a["id"] - b["id"];
            }
        );
    }
}