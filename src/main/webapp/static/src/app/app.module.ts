import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/index';
import { GaragesComponent } from './garages/index';
import { GarageComponent } from './garage/index';

import { CarComponent } from './car/index';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GarageService, CarService, WebApiService } from './service/index';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarComponent,
    GaragesComponent,
    GarageComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    })
  ],
  providers: [
    WebApiService,
    GarageService,
    CarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
