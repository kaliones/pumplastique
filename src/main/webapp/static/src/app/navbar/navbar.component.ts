import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    moduleId: module.id,
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {
    isCollapsed = true;
    constructor(private route: ActivatedRoute, private router: Router) {
    }

}