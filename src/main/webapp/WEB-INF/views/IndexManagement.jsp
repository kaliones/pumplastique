<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">

	<head>
	  <meta charset="utf-8">
	  <title>Pumplastique</title>
	  <base href="/" />

	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="icon" type="image/x-icon" href="favicon.ico">
	</head>

	<body>
	  <app-root></app-root>
	<script type="text/javascript" src="<c:url value='/static/dist/inline.bundle.js' />"></script><script type="text/javascript" src="<c:url value='/static/dist/polyfills.bundle.js' />"></script><script type="text/javascript" src="<c:url value='/static/dist/scripts.bundle.js' />"></script><script type="text/javascript" src="<c:url value='/static/dist/styles.bundle.js' />"></script><script type="text/javascript" src="<c:url value='/static/dist/vendor.bundle.js' />"></script><script type="text/javascript" src="<c:url value='/static/dist/main.bundle.js' />"></script></body>
	<footer class="mt-3 pt-1 pb-1 footer bg-primary text-white">
	  <div class="container text-center">
		Ancel Alexis
	  </div>
	</footer>

</html>

