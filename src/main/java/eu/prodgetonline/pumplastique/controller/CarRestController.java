package eu.prodgetonline.pumplastique.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import eu.prodgetonline.pumplastique.dao.CarDAO;
import eu.prodgetonline.pumplastique.dao.DAOFactory;
import eu.prodgetonline.pumplastique.model.Car;

@RestController
@RequestMapping(value = "/car")
public class CarRestController {

	CarDAO carDAO = DAOFactory.getCarDAO();

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<Car>> getAll() {
		return new ResponseEntity<ArrayList<Car>>(carDAO.getAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Car> get(@PathVariable("id") long id) {
		Car car = carDAO.get(id);
		if (car == null) {
			return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Car>(car, HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Car> create(@RequestBody Car car, UriComponentsBuilder ucBuilder) {
		carDAO.persist(car);

		return new ResponseEntity<Car>(car, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Car> update(@PathVariable("id") long id, @RequestBody Car car) {
		Car currentCar = carDAO.get(id);

		if (car == null) {
			return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
		}

		currentCar.setName(car.getName());
		currentCar.setBrand(car.getBrand());
		currentCar.setNbDoors(car.getNbDoors());

		carDAO.update(currentCar);
		return new ResponseEntity<Car>(currentCar, HttpStatus.OK);
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Car> delete(@PathVariable("id") long id) {
		Car currentCar = carDAO.get(id);
		
		if (currentCar == null) {
			return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
		}

		carDAO.remove(currentCar);
		return new ResponseEntity<Car>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	public ResponseEntity<Car> deleteAll() {

		carDAO.removeAll();
		return new ResponseEntity<Car>(HttpStatus.NO_CONTENT);
	}

}