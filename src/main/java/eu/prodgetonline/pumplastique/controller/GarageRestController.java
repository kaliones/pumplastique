package eu.prodgetonline.pumplastique.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.xml.bind.DataBindingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import eu.prodgetonline.pumplastique.dao.CarDAO;
import eu.prodgetonline.pumplastique.dao.DAOFactory;
import eu.prodgetonline.pumplastique.dao.GarageDAO;
import eu.prodgetonline.pumplastique.model.Car;
import eu.prodgetonline.pumplastique.model.Garage;

@RestController
@RequestMapping(value = "/garage")
public class GarageRestController {

	GarageDAO garageDAO = DAOFactory.getGarageDAO();
	CarDAO carDAO = DAOFactory.getCarDAO();

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<Garage>> getAll() {
		return new ResponseEntity<ArrayList<Garage>>(garageDAO.getAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Garage> get(@PathVariable("id") long id) {
		Garage garage = garageDAO.get(id);
		if (garage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Garage>(garage, HttpStatus.OK);
	} 

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Garage> create(@RequestBody Garage garage, UriComponentsBuilder ucBuilder) {
		
		garageDAO.persist(garage);

		//HttpHeaders headers = new HttpHeaders();
		//headers.setLocation(ucBuilder.path("/garage/{id}").buildAndExpand(garage.getId()).toUri());
		return new ResponseEntity<Garage>(garage, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Garage> update(@PathVariable("id") long id, @RequestBody Garage garage) {
		Garage currentGarage = garageDAO.get(id);

		if (garage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}

		currentGarage.setName(garage.getName());
		currentGarage.setNbOfPlaces(garage.getNbOfPlaces());

		garageDAO.update(currentGarage);
		return new ResponseEntity<Garage>(currentGarage, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}/car", method = RequestMethod.POST)
	public ResponseEntity<?> addCar(@PathVariable("id") long id, @RequestBody Car car) {
		Garage currentGarage = garageDAO.get(id);
		Car currentCar = carDAO.get(car.getId());

		if (currentCar == null) {
			return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
		}
		if (currentGarage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}
		try {
			currentGarage.addCar(currentCar);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_ACCEPTABLE);
		} catch (DataFormatException e) {
			return new ResponseEntity<Car>(HttpStatus.CONFLICT);
		}
		garageDAO.update(currentGarage);
		return new ResponseEntity<Garage>(currentGarage, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}/car", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeCar(@PathVariable("id") long id, @RequestBody Car car) {
		Garage currentGarage = garageDAO.get(id);
		Car currentCar = carDAO.get(car.getId());

		if (currentCar == null) {
			return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
		}
		if (currentGarage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}
		currentGarage.removeCar(currentCar);
		garageDAO.update(currentGarage);
		return new ResponseEntity<Void>( HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Garage> delete(@PathVariable("id") long id) {
		Garage currentGarage = garageDAO.get(id);

		if (currentGarage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}

		garageDAO.remove(currentGarage);
		return new ResponseEntity<Garage>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	public ResponseEntity<Garage> deleteAll() {

		garageDAO.removeAll();
		return new ResponseEntity<Garage>(HttpStatus.NO_CONTENT);
	}

}