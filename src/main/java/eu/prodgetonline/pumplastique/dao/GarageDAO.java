package eu.prodgetonline.pumplastique.dao;

import eu.prodgetonline.pumplastique.dao.interfaces.GarageDAOInt;
import eu.prodgetonline.pumplastique.model.Car;
import eu.prodgetonline.pumplastique.model.Garage;

public class GarageDAO extends AbstractJPA<Garage> implements GarageDAOInt {

	public GarageDAO() {
		super(Garage.class);
	}

	@Override
	public Garage get(long id) { 
		try {
			return this.data.stream().filter(obj -> ((Garage) obj).getId() == id).findFirst().get();
		} catch (Exception e) {
			return null;
		}
	} 

	@Override
	public boolean remove(Garage garage) {
		//reset link with garage for cars
		for (Car car : garage.getCars()) {
			garage.removeCar(car);
		}
		return super.remove(garage);
	}

	@Override
	public boolean removeAll() {
		//reset link with garage for cars
		for (Garage garage : this.data) {
			for (Car car : garage.getCars()) {
				garage.removeCar(car);
			}
		}
		return super.removeAll();
	}

	public void loadFixture() {
		this.data.add(new Garage("Reims Gare Centre", 200));
		this.data.add(new Garage("Pumplastique ", 40));
		this.data.add(new Garage("Reims petit parking ", 2));
		this.data.add(new Garage("Roissy Charles de Gaulle", 2500));
		this.data.add(new Garage("Nice Gare", 500));
	}
}
