package eu.prodgetonline.pumplastique.dao;

public class DAOFactory {
	//singleton by DAO because data is in memory with array in DAO class
	private static GarageDAO garageDAO;
	private static CarDAO carDAO;
	private DAOFactory() {
	}

	public static GarageDAO getGarageDAO() {
		if(DAOFactory.garageDAO == null)
		{
			DAOFactory.garageDAO = new GarageDAO();
		}
		return DAOFactory.garageDAO ;
	}

	public static CarDAO getCarDAO() {
		if(DAOFactory.carDAO == null)
		{
			DAOFactory.carDAO = new CarDAO();
		}
		return DAOFactory.carDAO ;
	}
}
