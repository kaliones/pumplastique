package eu.prodgetonline.pumplastique.dao;

import eu.prodgetonline.pumplastique.dao.interfaces.CarDAOInt;
import eu.prodgetonline.pumplastique.model.Car;
import eu.prodgetonline.pumplastique.model.Garage;

public class CarDAO extends AbstractJPA<Car> implements CarDAOInt {

	public CarDAO() {
		super(Car.class);
	}

	@Override
	public Car get(long id) {
		try {
			return this.data.stream().filter(obj -> ((Car) obj).getId() == id).findFirst().get();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean remove(Car car) {
		// reset link with car for garage

		if (car.getGarage() != -1) {
			Garage garage = DAOFactory.getGarageDAO().get(car.getGarage());
			garage.removeCar(car);
		}
		return super.remove(car);
	}

	@Override
	public boolean removeAll() {
		// reset link with car for garage
		for (Car car : this.data) {

			if (car.getGarage() != -1) {
				Garage garage = DAOFactory.getGarageDAO().get(car.getGarage());
				garage.removeCar(car);
			}
		}
		return super.removeAll();
	}

	public void loadFixture() {
		this.data.add(new Car("Mégane coupée", "Renault", (byte) 3));
		this.data.add(new Car("Mégane coupée", "Renault", (byte) 5));
		this.data.add(new Car("3008", "Peugeot", (byte) 5));
		this.data.add(new Car("Model S", "Tesla", (byte) 5));
	}
}
