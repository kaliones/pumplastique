package eu.prodgetonline.pumplastique.dao.interfaces;

import java.util.ArrayList;

public interface DAO<T> {

	boolean persist(T t);

	boolean update(T t);

	T get(long id);

	boolean remove(T t);

	boolean removeAll();
	
	ArrayList<T> getAll();

}