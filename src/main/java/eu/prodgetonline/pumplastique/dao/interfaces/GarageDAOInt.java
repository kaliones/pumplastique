package eu.prodgetonline.pumplastique.dao.interfaces;

import java.util.List;

import eu.prodgetonline.pumplastique.model.Garage;

public interface GarageDAOInt extends DAO<Garage>{

}