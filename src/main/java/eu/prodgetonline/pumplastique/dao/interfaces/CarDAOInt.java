package eu.prodgetonline.pumplastique.dao.interfaces;

import eu.prodgetonline.pumplastique.model.Car;

public interface CarDAOInt extends DAO<Car>{

}