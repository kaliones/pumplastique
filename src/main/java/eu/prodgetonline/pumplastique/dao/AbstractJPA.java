package eu.prodgetonline.pumplastique.dao;

import java.util.ArrayList;

import eu.prodgetonline.pumplastique.dao.interfaces.DAO;

public abstract class AbstractJPA<T> implements DAO<T> {
	protected ArrayList<T> data;

	protected Class<T> classT;

	public AbstractJPA(Class<T> classT) {
		this.classT = classT;
		this.data = new ArrayList<T>();
	}

	@Override
	public boolean persist(T t) {
		// if in list return true
		if (!this.data.contains(t)) {
			return this.data.add(t);
		}
		return true;
	}

	@Override
	public boolean update(T t) {
		if (this.data.contains(t)) {
			// if not remove true add did not
			return this.data.remove(t) && this.data.add(t);
		}
		return true;
	}

	@Override
	public abstract T get(long id) ;

	@Override
	public boolean remove(T t) {
		//if not in list return true
		if (this.data.contains(t)) {
			return this.data.remove(t);
		}
		return true;
	}

	@Override
	public boolean removeAll() {
		return this.data.removeIf(obj -> true);
	}

	@Override
	public ArrayList<T> getAll() {
		return this.data;
	}
}
