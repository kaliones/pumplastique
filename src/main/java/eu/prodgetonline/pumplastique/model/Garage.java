package eu.prodgetonline.pumplastique.model;

import java.util.ArrayList;
import java.util.zip.DataFormatException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.client.RestClientException;

public class Garage {
	private static long lastId = 0;
	private long id;

	private String name;
	private int nbOfPlaces;
	private ArrayList<Car> cars;

	public Garage() {
		this.id = Garage.lastId++;
		this.nbOfPlaces = 0;
		this.cars = new ArrayList<Car>();
	}

	public Garage(String name, int nbOfPlaces) {
		this.id = Garage.lastId++;
		this.name = name;
		this.nbOfPlaces = nbOfPlaces;
		this.cars = new ArrayList<Car>();
	}

	public Garage(long id, String name, int nbOfPlaces) {
		this.id = id;
		this.name = name;
		this.nbOfPlaces = nbOfPlaces;
		this.cars = new ArrayList<Car>();
	}

	public Garage(long id, String name, int nbOfPlaces, ArrayList<Car> cars) {
		this.id = id;
		this.name = name;
		this.nbOfPlaces = nbOfPlaces;
		this.cars = cars;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNbOfPlaces() {
		return this.nbOfPlaces;
	}

	public void setNbOfPlaces(int nbOfPlaces) {
		this.nbOfPlaces = nbOfPlaces;
	}

	// for read only
	public ArrayList<Car> getCars() {
		return new ArrayList<Car>(this.cars);
	}

	public void setCars(ArrayList<Car> cars) {
		this.cars = new ArrayList<Car>(cars);
	}

	public boolean addCar(Car car) throws DataFormatException,DataIntegrityViolationException {
		if (this.nbOfPlaces <= this.cars.size()) {
			throw new DataIntegrityViolationException("The garage has no more place");
		} else if (car.getGarage() != -1) {
			throw new DataFormatException("The car is already in a parking lot");
		}
		// if in list return true
		if (!this.cars.contains(car)) {
			car.setGarage(this.id);
			return this.cars.add(car);
		}
		return true;
	}

	public boolean updateCar(Car car) {
		// if in list return true
		if (this.cars.contains(car)) {
			// if not remove true add did not
			return this.cars.remove(car) && this.cars.add(car);
		}
		return true;
	}

	public boolean removeCar(Car car) {
		// if not in list return true
		if (this.cars.contains(car)) {
			car.setGarage(-1);
			return this.cars.remove(car);
		}
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || !(obj instanceof Garage)) {
			return false;
		}
		Garage other = (Garage) obj;
		// unique test in id because id is only unique value
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Garage " + this.name;
	}

}
