package eu.prodgetonline.pumplastique.model;

import java.util.List;

public class Car {
	private static long lastId = 0;
	private long id;

	private String name;
	private String brand;
	private byte nbDoors;
	private long garage;

	public Car() {
		this.id = Car.lastId++;
		this.garage = -1;
	}

	public Car( String name, String brand, byte nbDoors) {
		this.id = Car.lastId++;
		this.name = name;
		this.brand = brand;
		this.nbDoors = nbDoors;
		this.garage = -1;
	}

	public Car(long id, String name, String brand, byte nbDoors) {
		this.id = id;
		this.name = name;
		this.nbDoors = nbDoors;
		this.brand = brand;
		this.garage = -1;
	}

	public Car(long id, String name, String brand, byte nbDoors, long garage) {
		this.id = id;
		this.name = name;
		this.brand = brand;
		this.nbDoors = nbDoors;
		this.garage = garage;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGarage() {
		return this.garage;
	}

	public void setGarage(long garage) {
		this.garage = garage;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public byte getNbDoors() {
		return this.nbDoors;
	}

	public void setNbDoors(byte nbDoors) {
		this.nbDoors = nbDoors;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || !(obj instanceof Car)) {
			return false;
		}
		Car other = (Car) obj;
		// unique test in id because id is only unique value
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Car " + this.name;
	}

}
