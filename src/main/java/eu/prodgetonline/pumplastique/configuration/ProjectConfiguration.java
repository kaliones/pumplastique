package eu.prodgetonline.pumplastique.configuration;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import eu.prodgetonline.pumplastique.dao.CarDAO;
import eu.prodgetonline.pumplastique.dao.DAOFactory;
import eu.prodgetonline.pumplastique.dao.GarageDAO;
import eu.prodgetonline.pumplastique.model.Garage;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "eu.prodgetonline.pumplastique")
public class ProjectConfiguration extends WebMvcConfigurerAdapter{

	@Bean
	public InternalResourceViewResolver getInternalResourceViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

    @PostConstruct
    public void init() {
    	CarDAO carDAO = DAOFactory.getCarDAO();
    	carDAO.loadFixture();
    	GarageDAO garageDAO = DAOFactory.getGarageDAO();
    	garageDAO.loadFixture();
    	Garage garage = garageDAO.get(1);
    	Garage garage2 = garageDAO.get(0);
    	try
    	{
    		garage2.addCar(carDAO.get(1));
    		garage.addCar(carDAO.get(2));
    		garage.addCar(carDAO.get(3));
    	}
    	catch (Exception e) {
    		e.getMessage();
		}
    }
    
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
	}

}